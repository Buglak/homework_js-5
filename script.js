let obj = {
    name: "Aleks",
    lastName: "Bom",
    someOther: "somesome",
    properties: {
        id: 124532,
        idSecond: 424356
    }
}

function cloneObjFunc(obj) {
    let cloneObj = {};
    for(let elem in obj) {
        console.log(elem);
        if(typeof (obj[elem]) == "object") {
            // console.log("obj :", obj[elem]);
            cloneObj[elem] = cloneObjFunc(obj[elem]);
        } else {
            cloneObj[elem] = obj[elem];
        }
    }
    return cloneObj;
}
let objSecond = cloneObjFunc(obj);

console.log(objSecond);
